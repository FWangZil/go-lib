module gitlab.com/FWangZil/go-lib

go 1.15

require (
	github.com/Shopify/sarama v1.27.2
	github.com/casbin/casbin/v2 v2.19.8
	github.com/gogf/gf v1.14.6
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.1.2
	github.com/pkg/errors v0.9.1
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.7.1
	github.com/streadway/amqp v1.0.0
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.9
)
