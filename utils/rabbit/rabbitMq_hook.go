package rabbit

import (
	"log"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/FWangZil/go-lib/amqp/rabbitmq"

	"github.com/sirupsen/logrus"
)

type AMQPHook struct {
	AMQPServer   string
	Consumername string
	Password     string
	Exchange     string
	ExchangeType string
	RoutingKey   string
	VirtualHost  string
	Queue        string
	Mandatory    bool
	Immediate    bool
	Durable      bool
	Internal     bool
	NoWait       bool
	AutoDeleted  bool
	TTL          int
}

type AddHookParams struct {
	Server, Consumername, Password, Exchange, RoutingKey, ExchangeType, VirtualHost, Queue string
	TTL                                                                                    int
}

func NewAMQPHook(param AddHookParams) *AMQPHook {
	return NewAMQPHookWithType(param)
}

func NewAMQPHookWithType(param AddHookParams) *AMQPHook {
	hook := AMQPHook{}

	hook.AMQPServer = param.Server
	hook.Consumername = param.Consumername
	hook.Password = param.Password
	hook.Exchange = param.Exchange
	if param.ExchangeType == "" {
		hook.ExchangeType = "direct"
	} else {
		hook.ExchangeType = param.ExchangeType
	}
	hook.RoutingKey = param.RoutingKey
	hook.VirtualHost = param.VirtualHost
	hook.TTL = param.TTL
	hook.Queue = param.Queue

	hook.Durable = true

	return &hook
}

// Fire is called when an event should be sent to the message broker
func (hook *AMQPHook) Fire(entry *logrus.Entry) error {

	body, err := entry.String()
	if err != nil {
		log.Fatal(err)
	}

	ch, err := rabbitmq.Client.Conn.Channel()
	if err != nil {
		log.Fatal(errors.WithMessage(err, "the logrus to rabbit hook error"))
	}

	err = ch.Publish(
		hook.Exchange,
		hook.RoutingKey,
		hook.Mandatory,
		hook.Immediate,
		amqp.Publishing{
			ContentType: "json",
			Body:        []byte(body),
		})
	if err != nil {
		log.Fatal(errors.WithMessage(err, "the logrus to rabbit hook error"))
	}

	return nil
}

// Levels is available logging levels.
func (hook *AMQPHook) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
		logrus.WarnLevel,
		logrus.InfoLevel,
		logrus.DebugLevel,
		logrus.TraceLevel,
	}
}
