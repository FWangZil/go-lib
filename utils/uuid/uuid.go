package uuid

import (
	"strings"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

// NewUUID 生成新的uuid
func NewUUID() string {
	u2, err := uuid.NewUUID()
	var no string
	if err != nil {
		logrus.Errorln("NewUUID error is:", err.Error())
	} else {
		no = strings.Replace(u2.String(), "-", "", -1)
	}
	return no
}
